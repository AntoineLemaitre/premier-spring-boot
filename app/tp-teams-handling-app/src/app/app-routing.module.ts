import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// @ts-ignore
import {TeamListPageComponent} from "@team-handling/teams/containers/team-list-page/team-list-page.component";
// @ts-ignore
import {TeamDetailPageComponent} from "@team-handling/teams/containers/team-detail-page/team-detail-page.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'teams',
    pathMatch: 'full'
  },
  {
    path:'team/:id',
    component:TeamDetailPageComponent
  },


  {
    path: 'teams',
    component: TeamListPageComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
