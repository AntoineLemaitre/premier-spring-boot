import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Team} from "@team-handling/teams/models/team";
import {environment} from "@team-handling-env/environment";


@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private http: HttpClient) {
  }


  getTeams(): Observable<Team[]> {
    return this.http.get<Team[]>(`http://localhost:8080/teams`);

  }
  getTeamsById(id:number): Observable<Team> {
    return this.http.get<Team>(`${environment.backendPoint}/team/${id}`);

  }
}
