import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AppComponent} from "../app.component";
import {BrowserModule} from "@angular/platform-browser";
import {AppRoutingModule} from "@team-handling/app-routing.module";
import { TeamListPageComponent } from './containers/team-list-page/team-list-page.component';
import {MatCardModule} from "@angular/material/card";
import { TeamDetailPageComponent } from './containers/team-detail-page/team-detail-page.component';
import {MatButtonModule} from "@angular/material/button";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    //AppComponent
    TeamListPageComponent,
    TeamDetailPageComponent],
  exports: [
    TeamListPageComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    RouterModule


  ]
})
export class TeamsModule { }
